# Postfix configuration for Debian 10

Mail server configuration with Postfix, Dovecot and some additional modules (Spamassassin etc...)

### Installation of packages:

`root@mailsrv:~# apt install postfix sasl2-bin dovecot-core dovecot-pop3d dovecot-imapd`

In configuration wizard choose:
Internet site / No configuration

### Sample Postfix configuration:

For examples I have used the vim editor, you can choose also **nano**

`root@mailsrv:~# vim /etc/postfix/main.cf` 

for ipv4 only please set:

`mynetworks = 127.0.0.0/8`

Add below code:

```
# SMTP-Auth settings
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_auth_enable = yes
smtpd_sasl_security_options = noanonymous
smtpd_sasl_local_domain = $myhostname
smtpd_recipient_restrictions = permit_mynetworks, permit_auth_destination, permit_sasl_authenticated, reject 
```

save & quit


`root@mailsrv:~# newaliases`

`root@mailsrv:~# systemctl restart postfix`

